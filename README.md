# COLA2 LIB ROS

Reusable code with ROS that is used all over the COLA2 software architecture with  (c++ & python).

## Documentation

You can check the documentation API for this library [here](http://api.iquarobotics.com/202401/cola2_lib_ros/).
